//: Please build the scheme 'RxSwiftPlayground' first
import RxSwift

example(of: "IgnoringElements") {
    let strikes = PublishSubject<String>()
    let disposeBag = DisposeBag()
    strikes.ignoreElements().subscribe { _ in
        print("you are out")
        }.addDisposableTo(disposeBag)
    
    strikes.onNext("X")
    strikes.onNext("X")
    strikes.onNext("X")
    strikes.onCompleted()
}

example(of: "elementAt") {
    let strikes = PublishSubject<String>()
    let disposeBag = DisposeBag()
    strikes.elementAt(0).subscribe { event in
        if let value = event.element {
            print(value)
        } else {
            print(event)
        }
        }.addDisposableTo(disposeBag)
    strikes.onNext("0")
    strikes.onNext("1")
    strikes.onNext("2")
    strikes.onNext("3")
    strikes.onCompleted()
}

example(of: "filter") {
    let disposeBag = DisposeBag()
    let integers = Observable.from([1, 2, 3, 4, 5, 6, 7])
    integers.filter{$0%2 == 0}.subscribe(onNext: {
        print($0)
    }).addDisposableTo(disposeBag)
}

example(of: "skip") {
    let disposeBag = DisposeBag()
    Observable.of("A", "B", "C", "D", "E", "F").skip(2).subscribe(onNext: {
        print($0)
    }).addDisposableTo(disposeBag)
}

example(of: "skip until") {
    let disposeBag = DisposeBag()
    let subject = PublishSubject<String>()
    let trigger = PublishSubject<String>()
    subject
        .skipUntil(trigger)
        .subscribe(onNext: {
            print($0)
        })
        .addDisposableTo(disposeBag)
    subject.onNext("A")
    subject.onNext("B")
    trigger.onNext("X")
    subject.onNext("C")
}

example(of: "take") {
    let disposeBag = DisposeBag()
    Observable.of(1, 2, 3, 4, 5).take(2).subscribe(onNext: {
        print($0)
    }).addDisposableTo(disposeBag)
}

example(of: "takeWhileWithIndex") {
    let disposeBag = DisposeBag()
    Observable.of("1", "2", "3", "4").takeWhileWithIndex({
        print($0)
        return $1 % 2 == 0
    }).subscribe(onNext: { _ in
        //print($0)
    }).addDisposableTo(disposeBag)
}

example(of: "distinctUntilChanged") {
    let disposeBag = DisposeBag()
    // 1
    Observable.of("A", "A", "B", "B", "A")
        // 2
        .distinctUntilChanged()
        .subscribe(onNext: {
            print($0) })
        .addDisposableTo(disposeBag)
}
/*:
 Copyright (c) 2014-2016 Razeware LLC
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */
