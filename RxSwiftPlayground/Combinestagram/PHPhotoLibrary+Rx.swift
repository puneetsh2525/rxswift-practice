//
//  PHPhotoLibrary+Rx.swift
//  Combinestagram
//
//  Created by Puneet Sharma2 on 24/05/18.
//

import Foundation
import Photos
import RxSwift

extension PHPhotoLibrary {
    static var isAuthorized:Observable<Bool> {
        return Observable.create{ observer in
            DispatchQueue.main.async {
                if authorizationStatus() == PHAuthorizationStatus.authorized {
                    observer.onNext(true)
                    observer.onCompleted()
                } else {
                    observer.onNext(false)
                    requestAuthorization({ (status) in
                        observer.onNext(status == PHAuthorizationStatus.authorized)
                        observer.onCompleted()
                    })
                }
            }
            return Disposables.create()
        }
    }
}
