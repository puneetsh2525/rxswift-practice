//
//  UIAlertController+Error.swift
//  Combinestagram
//
//  Created by Puneet Sharma2 on 26/05/18.
//

import Foundation
import UIKit
import RxSwift

extension UIViewController {
    func alert(title: String, description: String? = nil) -> Observable<Void> {
        return Observable.create { [weak self] observer in
            let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)
            let closeAction = UIAlertAction.init(title: "Close", style: .default, handler: { _ in
                return observer.onCompleted()
            })
            alert.addAction(closeAction)
            self?.present(alert, animated: true, completion: nil)
            return Disposables.create {
                self?.dismiss(animated: true, completion: nil)
            }
        }
    }
}
